# V8 Sim
Simple Car Engine and Car State simulator app with basic controls provided to manipulate state of several components.

### PURPOSE

The purpose is to showcase the skills and knowledge of `React` Development using `Redux in a `Flux` pattern application, as well as illustrate my understanding of a project of a certain nature.

### NOTE:

* Application is `not tested` and tests are pending to be implemented in the future.
* Application `Unit` and `Functional` testing was not the purpose of this exercise.
* Application was not designed with Graphics and UI Authoring in mind as it serves a different purpose.
 
### Usage:
* Clone:
* Run: npm install
* Run: npm start - browser window will open for dev view
* Else: launch dis content on web server

 

# Simulated components
* `Engine Power Block` That assembles several parts together. Like cylinders and Ignition Coils
* `Cylinders` V8 with 4 stroke positioning (See positioning on the panel display). Each power stroke "0" fuel is consumed.
* `Ignition Coils` for each cylinder that is fired with timing delay on cylinder power stroke condition. Each coil fire consumes Batter change.
* `Spark plugs` for each coil. Fires after some time if its coil is discharged, leading to power stroke of its cylinder to commence.
* `Fuel Tank`. With low fuel and fuel quality conditions. Each power stroke of a cylinder will consume a predefined amount of fuel
* `Battery`. With charge and low conditions. If Low condition is reached - the `Low` indicator will appear
* `Generator` Recharges the battery during run time. Has condition indicator that will diminish with usage.

`To see better what is going on - use lower RPM.`

All components can be animated using standard HTML/CSS techniques, however, animating was not the purpose of this exercise.


## Controls
* `Accelerator`. Once running, use accelerator control to change the engine RPM. If RPM exceeds red line condition - the `"RED"` indicator will show up next to power block panel.
* `Fuel`. Change amount of fuel to trigger different conditions
* `Battery`. Charge can be set using controller to trigger different conditions
* `Generator`. Condition of Generator can be set to trigger different conditions
* `Start/Stop`. Use "start/stop" start/stop engine. Once running - you should see the RPM sitting at the lowest setting.

 
 ## Well, thats about covers it!

 Remember to inspect the code and see console logs.

 All logs are set to debug/warn, so be sure to enable verbose level in your debugger.
 
 ### NOTE
* Due to Timer limitations in `Javascript` - high RPM may not result in change in frame rate.


 

 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
