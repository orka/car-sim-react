import React, {Component} from 'react';
import './style/index.scss';
import Panel from './components/panel';
import {Engine} from './car/engine';
import {Header} from './components/header';
import {Footer} from './components/footer';
import {Controls} from './components/controls';

class App extends Component {
    constructor(props) {
        super(props);
        this.engine = new Engine(this.props.store);
    }

    engineStart() {
        console.log('starting engine');
        // this.props.store.dispatch(engineStart())
    }

    engineStop() {
        console.log('stopping engine');
        // this.props.store.dispatch(engineStop())
    }

    componentDidMount() {
        // this.engineStart(); setTimeout(()=>this.engineStop(), 50000)
    }
    render() {
        return (
            <div className="App">
                <Header
                    name="Basic car panel and engine event simulator app"
                    purpose="Showcase React and Redux knowledge in Flux flow"
                    author="Sam Avanesov"
                    email="sam.avanesoff@gmail.com"
                    phone="+1-613-884-4178"
                    li="https://linkedin.com/in/samvel-avanesov"
                    source="https://gitlab.com/orka/car-sim-react"/>

                <div className="dash">
                    <Panel/>
                    <Controls/>
                </div>
                <Footer
                    info="Use Engine controls to change state! Best to examine the code to understand the sequence of events. Total time spent: 20h."/>
            </div>
        );
    }
}

export default App;