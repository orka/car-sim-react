export const TURN_ALTERNATOR = 'TURN_ALTERNATOR';
export function turnAlternator(charge) {
    return {type: TURN_ALTERNATOR, charge: charge}
}

export const STOP_ALTERNATOR = 'STOP_ALTERNATOR';
export function stopAlternator() {
    return {type: STOP_ALTERNATOR}
}

export const SET_ALTERNATOR_CONDITION = 'SET_ALTERNATOR_CONDITION';
export function setAlternatorCondition(value) {
    return {type: SET_ALTERNATOR_CONDITION, value: value}
}