export const USE_BATTERY = 'USE_BATTERY';
export function useBattery(amount) {
    return {type: USE_BATTERY, amount: amount}
}

export const CHARGE_BATTERY = 'CHARGE_BATTERY';
export function chargeBattery(amount) {
    return {type: CHARGE_BATTERY, amount: amount}
}

export const SET_BATTERY = 'SET_BATTERY';
export function setBattery(value) {
    return {type: SET_BATTERY, value: value}
}