export const ENGINE_START = "ENGINE_START";
export function engineStart() {
    return {type: ENGINE_START}
}

export const ENGINE_STOP = "ENGINE_STOP";
export function engineStop() {
    return {type: ENGINE_STOP}
}

export const ENGINE_CRANKSHAFT_POSITION = "ENGINE_CRANKSHAFT_POSITION";
export function turnCrankshaft(crankPosition, cylinderPositions, rpm, timing) {
    return {type: ENGINE_CRANKSHAFT_POSITION, crankshaftPosition: crankPosition, cylindersPositions: cylinderPositions, rpm: rpm, timing: timing}
}

export const ENGINE_POWER_UP = "ENGINE_POWER_UP";
export function enginePowerUp(amount) {
    return {type: ENGINE_POWER_UP, amount: amount}
}

export const ENGINE_POWER_DOWN = "ENGINE_POWER_DOWN";
export function enginePowerDown(amount) {
    return {type: ENGINE_POWER_DOWN, amount: amount}
}

export const ENGINE_RPM = "ENGINE_RPM";
export function setRpm(value) {
    return {type: ENGINE_RPM, value: value}
}

export const ENGINE_SPARKPLUG_FIRE = 'ENGINE_SPARKPLUG_FIRE';
export function fireSparkPlug(position, energyUsage) {
    return {type: ENGINE_SPARKPLUG_FIRE, position: position, energyUsage: energyUsage}
}

export const ENGINE_IGNITION_COIL_FIRE = 'ENGINE_IGNITION_COIL_FIRE';
export function fireIgnitionCoil(position, energyUsage) {
    return {type: ENGINE_IGNITION_COIL_FIRE, position: position, energyUsage: energyUsage}
}

export const CYLINDER_FIRE = 'CYLINDER_FIRE';
export function fireCylinder(position, power, fuelConsumption) {
    return {type: CYLINDER_FIRE, position: position, power: power, fuelConsumption: fuelConsumption}
}
