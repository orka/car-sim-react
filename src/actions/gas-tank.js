export const USE_GAS = 'USE_GAS';
export function useGas(amount) {
    return {type: USE_GAS, amount: amount}
}

export const ADD_GAS = 'ADD_GAS';
export function addGas(amount) {
    return {type: ADD_GAS, amount: amount}
}

export const SET_FUEL = 'SET_FUEL';
export function setFuel(amount) {
    return {type: SET_FUEL, amount: amount}
}