/**
 * Alternator uses crankshaft power turns and charges the battery
 * This is energy generator
 */
import {turnAlternator} from '../actions/alternator';
//
export default class Alternator {
    position = null; //crankshaft position
    chargePerCrank = 0.001;
    //
    constructor(props) {
        this.store = props.store;
        this.unsub = this
            .store
            .subscribe(_ => this.onStateChange())
    }

    onStateChange() {
        const state = this
            .store
            .getState()
            .engineState;

        if (this.position !== state.crankshaftPosition) {
            this.position = state.crankshaftPosition; // set new position
            this
                .store
                .dispatch(turnAlternator(this.chargePerCrank));
        }
    }
}