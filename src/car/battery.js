/**
 * Battery is the source of power
 * it is charged by the alternator
 * and used by all the electric processes of car
 */

import {} from '../actions/battery';

export default class Battery {
    charge = null;
    constructor(props) {
        this.store = props.store;
        this.unsub = this
            .store
            .subscribe(_ => this.onStateChange())
    }

    onStateChange() {
        const state = this
            .store
            .getState()
            .batteryState;

        if (this.charge !== state.charge) {

            this.charge = state.charge;

            console.debug('Battery charge: ', this.charge);
        }

        if (this.charge === 0) {
            console.warn('Battery Drained')
        }
    }

    getCharge() {
        return this.charge;
    }

}