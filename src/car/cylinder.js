/**
 * Cylinder will check each its stroke
 * If its it a power stro
 */

import Sparkplug from './sparkplug';
import {fireCylinder} from '../actions/engine';
import {useGas} from '../actions/gas-tank';
/**
  *
  */
export default class Cylinder {
    position = null;
    stroke = null;
    fuelUsage = 0.0005;
    fuelTank = null;
    fired = false;
    hasFuel = true; // should really spin the crank few times to add fuel
    timingRatio = 2;
    timer = null;
    /**
     *
     * @param {*} props
     */
    constructor(props) {
        this.store = props.store;
        this.position = props.position;
        this.stroke = props.stroke;
        //init Sparkplug
        this.sparkplug = new Sparkplug({position: this.position, store: this.store})
        //respond to position
        this.unsubscribe = this
            .store
            .subscribe(_ => this.onStateChange());
    }

    onStateChange() {
        const state = this
            .store
            .getState();

        //set delay for visualization
        this.timing = state.engineState.timing / this.timingRatio;
        this.fuelTank = state.fuelState;
        //when crankshaft is turned - check the positions and fire if in power stroke
        const newStroke = state.engineState.cylindersPositions[this.position];

        this.onStroke(newStroke, state.engineState.sparkplugFired);
    }

    getStroke() {
        return this.stroke;
    }

    onStroke(stroke, sparkplugFired) {
        //assign new stroke
        this.stroke = stroke;
        switch (stroke) {
            case 0:
                if (!this.fired && sparkplugFired === this.position && this.hasFuel) {
                    console.debug('firing cylinder', this.position);
                    this.fired = true; // set as fired
                    this.hasFuel = false;
                    //for visualization
                    clearTimeout(this.timer);
                    this.timer = setTimeout(_ => {
                        this
                            .store
                            .dispatch(fireCylinder(this.position, this.fuelTank.quality, this.fuelUsage));
                    }, this.timing);

                } else if (this.fired && sparkplugFired !== this.position) {
                    this.fired = false;
                }
                return;
            case 1: //exhaust stroke
                this.fired = false;
                return;
            case 2: //intake stroke - consume gas
                this.fired = false;
                if (!this.hasFuel && this.fuelTank.amount >= this.fuelUsage) {
                    this.hasFuel = true;
                    //for visualization
                    clearTimeout(this.timer);
                    this.timer = setTimeout(_ => {
                        this
                            .store
                            .dispatch(useGas(this.fuelUsage));
                    }, this.timing);
                }
                return;
            case 3: //compression stroke
                this.fired = false;
                return;
            default:
                return;
        }
    }

}