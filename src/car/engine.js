/*
 * engine event emitter
 * this is an engine signal generator,
 * similar to how computer would read out engine state from this module
 *
 */

import PowerBlock from './powerblock';
import Battery from './battery';
import Alternator from './alternator'; //generator

//
export class Engine {

    running = false;
    //
    constructor(store, props) {
        this.store = store;

        this.battery = new Battery({store: this.store})

        this.alternator = new Alternator({store: this.store})

        this.powerBlock = new PowerBlock({store: this.store})

        // changes
        this.unsubscribe = this
            .store
            .subscribe(_ => this.onStateChange());
    }

    onStateChange() {
        //retrieve engine state in a flux pattern
        const state = this
            .store
            .getState();

        //respond to start stop
        this.setEngineRunState(state.engineState.running);
    }

    /**
     * Respond to running state
     */
    setEngineRunState(running) {
        if (running === true && this.running !== true) {
            this.start();
        } else if (running === false && this.running === true) {
            this.stop();
        }
    }

    start() {
        this.running = true;
        this
            .powerBlock
            .start();
        //
        console.log('Engine started');
    }

    stop() {
        this.running = false;
        this
            .powerBlock
            .stop();
        console.log('Engine stopped');
    }

}