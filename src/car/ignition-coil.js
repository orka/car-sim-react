/**
 * Ignition Coil
 * Charges and sets off sparkplug
 * Uses energy
 */
import {fireIgnitionCoil} from '../actions/engine';
//
export default class IgnitionCoil {
    position = null;
    energyUsage = 0.001;
    timingRatio = 10;
    availablePower = null;
    condition = null;
    stroke = null;
    timer = null;
    fired = false;
    //
    constructor(props) {
        this.store = props.store;
        this.position = props.position;
        this.unsub = this
            .store
            .subscribe(_ => this.onStateChange())
    }

    onStateChange() {
        const state = this
            .store
            .getState();
        //set charge timing ratio for visualization
        this.timing = state.engineState.timing / this.timingRatio;
        //
        this.availablePower = state.batteryState.charge;
        //when crankshaft is turned - check the positions and fire if in power stroke
        const cylinderStroke = state.engineState.cylindersPositions[this.position];
        // debugger; because the crank is turning we enter in a different strokes per
        // cylinder if power stroke - then fire once!
        if (this.fired === false && cylinderStroke === 0) {
            this.fire();
        } else if (cylinderStroke > 0) {
            this.fired = false;
        }
    }

    fire(force) {
        console.debug('firing ignition coil', this.position);
        if (this.availablePower >= this.energyUsage) {
            if (!force && this.fired) {
                console.warn('Double Fire detected');
                return;
            }
            this.fired = true;
            clearTimeout(this.timer);
            //
            this.timer = setTimeout(() => {
                console.debug('firing coil', this.position);
                //fire
                this
                    .store
                    .dispatch(fireIgnitionCoil(this.position, this.energyUsage));

            }, this.timing);
        } else {
            console.warn('Unable to fire ignition coil! Not enough Energy!');
        }
    }
}