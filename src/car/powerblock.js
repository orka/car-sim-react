/*
 * Power Block of engine
 * this is an power stroke signal generator,
 * similar to how computer would read out engine state from this module
 *
 */

import {turnCrankshaft} from '../actions/engine';
import Cylinder from './cylinder';
import IgnitionCoil from './ignition-coil';
//
export default class PowerBlock {
    timing = 500;
    rpm = null;
    timeout = null;
    crankshaftPositions = 4; // for 4 stroke type engine
    crankshaftCurrentPosition = 0;
    running = false;
    cylinders = []; //collection of all cylinders
    ignitionCoils = []; //collection of all ignition coils
    cylinderFired = []; //collection of stroke fired sparkplugs
    //
    constructor(props) {
        this.store = props.store;
        this.setCylinders();
        this.setIgnitionCoils();
        // changes
        this.unsubscribe = this
            .store
            .subscribe(_ => this.onStateChange());
    }

    /**
     * create array of cylinders with their own initial positions subscribe to state
     */
    setCylinders(battery) {
        this.cylinders = this
            .store
            .getState()
            .engineState
            .cylindersPositions
            .map((v, i) => {
                return new Cylinder({
                    store: this.store, //initial stroke
                    stroke: v, // initial stroke state
                    position: i
                });
            });
    }

    setIgnitionCoils() {
        this.ignitionCoils = this
            .store
            .getState()
            .engineState
            .cylindersPositions
            .map((v, i) => {
                return new IgnitionCoil({
                    store: this.store, //initial stroke
                    position: i
                });
            });
    }

    onStateChange() {
        //retrieve engine state in a flux pattern
        const state = this
            .store
            .getState()
            .engineState;

        // set new crank position
        this.crankshaftCurrentPosition = state.crankshaftPosition;
        this.setTiming(state);
        this.checkPowerConditions(state);
    }

    setTiming(state) {
        //calculate rpm
        this.rpm = Math.max(state.min_rpm, Math.min(state.max_rpm, state.rpm));
        // infer timing per each crank position in ms from rpm (divide positions by 2
        // since the crank is done every 2 positions)
        this.timing = 1000 / (this.rpm / 60) / (this.crankshaftPositions / 2);
    }

    /**
     * Checks sparkplug fire conditions and turn the shaft
     */
    checkPowerConditions(state) {
        //X number of strokes require Y many sparkplugs to fire to turn the shaft
        const fireRequires = state.cylindersPositions.length / this.crankshaftPositions;
        let turned = false;
        //
        if (state.cylinderFire.length === fireRequires) {
            // remove already registered sparkplugs - if required length remaining - turn
            // the crank
            const newPositions = state
                .cylinderFire
                .filter(v => this.cylinderFired.indexOf(v) < 0)
                .length === fireRequires;

            if (newPositions === true) {
                //assign new sparkplugs that fired
                this.cylinderFired = state
                    .cylinderFire
                    .slice(0) //copy;
                //turn the shaft to new position
                this.onGotEnoughPower();
                turned = true;
            }
        }

        if (!turned) {
            console.debug('not enough power to turn');
        }
    }

    /**
     * Respond to running state
     */
    setEngineRunState(running) {
        if (running === true && this.running !== true) {
            this.start();
        } else if (running === false && this.running === true) {
            this.stop();
        }
    }

    start() {
        this.running = true;

        this
            .cylinders
            .forEach((c, i) => c.getStroke() === 0
                ? (this.ignitionCoils[i].fire(true), c.hasFuel = true, c.fired = false) //set off ignition coil of cylinder
                : null);
        //
        console.log('Power Block started');
    }

    stop() {
        this.running = false;
        console.log('Power Block stopped');
    }

    //when we have power - turn shaft
    onGotEnoughPower() {
        //always use timeouts to have a better control over engine timing
        clearTimeout(this.timeout);
        //assign timeout
        this.timeout = setTimeout(() => {
            //turn the shaft
            if (this.running === true) {

                this.turn();
            } else {
                //reset registered sparkplugs so we can restart the engine
                this.cylinderFired.length = 0;
            }
        }, this.timing);
    }

    /**
     * Generate RPM from timing
     */
    getRpm() {
        // const rpm = 1000 / (this.timing * (this.crankshaftPositions / 2)) * 60;
        return this.rpm;
    }

    getNextCrankPosition() {
        const position = (this.crankshaftCurrentPosition + 1) % this.crankshaftPositions;
        return position;
    }

    getNewCylinderPositions() {
        const cylinderPositions = this
            .cylinders
            .map(c => (c.getStroke() + 1) % this.crankshaftPositions);
        return cylinderPositions;
    }

    turn() {
        //turn the crankshaft to the next position
        const position = this.getNextCrankPosition();
        //get new cylinder positions
        const cylinderPositions = this.getNewCylinderPositions();

        const rpm = this.getRpm();
        console.debug('turn shaft');

        if (position % 2) {
            console.debug('full engine crank');
        }

        //dispatch state change in a flux pattern - top->down
        this
            .store
            .dispatch(turnCrankshaft(position, cylinderPositions, rpm, this.timing));
    }
}