/**
 *
 * sparkplug is set for each cylinder of the engine
 * each power stroke of each cylinder will dispatch action of sparkplug
 */
import {fireSparkPlug} from '../actions/engine';
//
export default class Sparkplug {
    position = null;
    energyUsage = 0.001;
    condition = null;
    timingRatio = 2;
    timer = null;
    fired = false;
    //
    constructor(props) {
        this.store = props.store;
        this.position = props.position;
        this.unsub = this
            .store
            .subscribe(_ => this.onStateChange())
    }

    onStateChange() {
        const state = this
            .store
            .getState()
            .engineState;
        //set charge timing ratio for visualization
        this.timing = state.timing / this.timingRatio;

        if (!this.fired && state.ignitionCoilFired === this.position) {
            this.fired = true;
            this.fire();
        } else if (state.ignitionCoilFired !== this.position) {
            this.fired = false;
        }
    }

    fire() {
        console.debug('firing plug', this.position);
        clearTimeout(this.timer);
        this.timer = setTimeout(_ => {
            this
                .store
                .dispatch(fireSparkPlug(this.position, this.energyUsage));
        }, this.timing)
    }
}