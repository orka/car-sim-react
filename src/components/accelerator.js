import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setRpm} from '../actions/engine';

export class Accelerator extends Component {

    getValue() {
        return this.props.running
            ? "Stop"
            : "Start";
    }

    render() {
        return (
            <div className="accelerator slider">
                <p>Accelerator:</p>
                <input
                    type="range"
                    min={0}
                    max={this.props.max}
                    value={this.props.value}
                    className="slider"
                    onChange={e => {
                    this
                        .props
                        .accelerate(e.target.value);
                }}/>
            </div>
        )
    }
}

//set map functions for redux to map state to props for our component
const mapStateToProps = state => {
    return {value: state.engineState.rpm, min: state.engineState.min_rpm, max: state.engineState.max_rpm}
}

//map actions to dispatch
const mapDispatchToProps = dispatch => {
    return {
        accelerate: rpm => {
            dispatch(setRpm(rpm))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Accelerator);