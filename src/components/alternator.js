import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setAlternatorCondition} from '../actions/alternator';

export class Alternator extends Component {

    render() {
        return (
            <div className="alternator slider">
                <p>Alternator:</p>
                <input
                    type="range"
                    min={this.props.min}
                    max={this.props.max}
                    value={this.props.condition}
                    step="0.05"
                    className="slider"
                    onChange={e => {
                    this
                        .props
                        .setAlternator(parseFloat(e.target.value));
                }}/>
            </div>
        )
    }
}

//set map functions for redux to map state to props for our component
const mapStateToProps = state => {
    return state.alternatorState
}

//map actions to dispatch
const mapDispatchToProps = dispatch => {
    return {
        setAlternator: amount => {
            dispatch(setAlternatorCondition(amount))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Alternator);