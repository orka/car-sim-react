import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setBattery} from '../actions/battery';

export class Battery extends Component {

    render() {
        return (
            <div className="battery slider">
                <p>Battery:</p>
                <input
                    type="range"
                    min={this.props.min}
                    max={this.props.max}
                    value={this.props.charge}
                    step="0.05"
                    className="slider"
                    onChange={e => {
                    this
                        .props
                        .setBattery(parseFloat(e.target.value));
                }}/>
            </div>
        )
    }
}

//set map functions for redux to map state to props for our component
const mapStateToProps = state => {
    return state.batteryState
}

//map actions to dispatch
const mapDispatchToProps = dispatch => {
    return {
        setBattery: amount => {
            dispatch(setBattery(amount))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Battery);