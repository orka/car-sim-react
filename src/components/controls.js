/**
 * Car Engine controls
 */
import React, {Component} from 'react';
import StartEngineButton from './start-engine-button';
import Accelerator from './accelerator';
import Fuel from './fuel';
import Battery from './battery';
import Alternator from './alternator';
import PowerBlockAnimation from './powerblock-animation';
export class Controls extends Component {

    render() {
        return (
            <div className="controls">
                <p className="intro">Press "Start" to start the engine. Use controls to change
                    components state. Have fun!</p>
                <StartEngineButton/>
                <div className="sliders">
                    <Accelerator/>
                    <Fuel/>
                    <Battery/>
                    <Alternator/>
                </div>
                <PowerBlockAnimation />
            </div>
        )
    }
}