import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Cylinder extends Component {

    strokes = [
        'power',
        'exhaust',
        'intake',
        'compression'
    ]

    constructor(props) {
        super(props);

        //state is local or encapsulated
        //no other component is aware of this state
        //“top-down” or “unidirectional” data flow.
        this.state = {
            on: this.props.on,
            stroke: this.props.stroke
        };

        //use set state to change the state
    }

    onNextStroke(){
        this.setState((state, props) => {
            return {
                //enter next stroke
                stroke: (state.stroke + 1) % this.strokes.length
            }
        });
    }

    componentDidMount() {
        //runs after the component output has been rendered to the DOM
    }

    componentWillUnmount() {}

    render() {
        //use set state with assigning function to battle ascync

        /*
        this.setState((state, props) => ({
            counter: state.counter + props.increment
          }));
        */

        return (
            <p>
                This cylinder is ON {this.state.on}, position down {!!this.state.down}
            </p>
        )
    }
}


//Set up prop types check for react
Cylinder.propTypes = {
    on:PropTypes.bool.isRequired,
    down:PropTypes.bool.isRequired
}


// Cylinder.defaultProps = {
//     on:false,
//     down:false
// }



export default Cylinder;