import React, {Component} from 'react';

/**
 * App Footer
 * Says project name, purpose and author info
 */
export class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info: props.info
        }
    }

    render() {
        return (
            <div className="App-footer">
                <div className="footer-content">
                    <div className="footer-info">
                        <p>{this.state.info}</p>
                    </div>
                </div>
            </div>
        )
    }
}