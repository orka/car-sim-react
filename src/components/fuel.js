import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setFuel} from '../actions/gas-tank';

export class Fuel extends Component {

    render() {
        return (
            <div className="fuel slider">
                <p>Fuel:</p>
                <input
                    type="range"
                    min={this.props.min}
                    max={this.props.max}
                    value={this.props.amount}
                    step="0.05"
                    className="slider"
                    onChange={e => {
                    this
                        .props
                        .setFuel(parseFloat(e.target.value));
                }}/>
            </div>
        )
    }
}

//set map functions for redux to map state to props for our component
const mapStateToProps = state => {
    return state.fuelState
}

//map actions to dispatch
const mapDispatchToProps = dispatch => {
    return {
        setFuel: amount => {
            dispatch(setFuel(amount))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Fuel);