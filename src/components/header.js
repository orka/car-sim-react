import React, {Component} from 'react';

/**
 * App Header
 * Says project name, purpose and author info
 */
export class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            purpose: props.purpose,
            author: props.author,
            phone: props.phone,
            email: props.email,
            li: props.li,
            source: props.source
        }
    }

    render() {
        return (
            <div className="App-header">
                <div className="header-content">
                    <div className="header-title">
                        <h1 >
                            <span>{this.state.name}</span>
                        </h1>
                    </div>
                    <div className="header-info">
                        <p>purpose:&nbsp;
                            <span className="bold">{this.state.purpose}</span>
                        </p>
                    </div>
                    <div className="header-contact">
                        <p>by:&nbsp;
                            <span className="bold">{this.state.author}</span>
                            <span className="bold">
                                &nbsp;(<a href={"tel:" + this.state.phone}>{this.state.phone}</a>)
                            </span>
                            <span className="bold">
                                &nbsp;(<a target="blank" href={this.state.li}>LinkedIn</a>)
                            </span>
                            <span className="bold">
                                &nbsp;(<a target="blank" href={"mailto:" + this.state.email}>{this.state.email}</a>)
                            </span>
                            <span className="bold">
                                &nbsp;(<a target="blank" href={this.state.source}>source</a>)
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}