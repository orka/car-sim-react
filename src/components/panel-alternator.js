import React, {Component} from 'react';
import {connect} from 'react-redux';

class Alternator extends Component {
    constructor(props) {
        super(props);
        //
        this.state = {
            engine: 0,
            on: false
        }
    }

    getCondition() {
        return this
            .props
            .alternator
            .condition
            .toFixed(3)
    }

    isBadCondition() {
        return this.props.alternator.condition <= this.props.alternator.low_condition
    }

    render() {
        return (
            <div className="alternator panel-item">
                <p
                    className={"title " + (this.props.alternator.running
                    ? 'active'
                    : '')}>Generator {this.isBadCondition()
                        ? <span className="warning">Replace</span>
                        : ''}</p>
                <ul>
                    <li>
                        Condition:
                        <span className="bold">{this.getCondition()}</span>
                    </li>
                </ul>
            </div>
        );
    }
}

//map engine state to cylinder positions props
const mapStateToProps = state => {
    return {alternator: state.alternatorState}
}

export default connect(mapStateToProps)(Alternator);
