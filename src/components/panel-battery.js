import React, {Component} from 'react';
import {connect} from 'react-redux';

class Battery extends Component {

    getCharge() {
        return (this.props.battery.charge * 13).toFixed(2) + 'V';
    }

    getCondition() {
        return this
            .props
            .battery
            .condition
            .toFixed(2);
    }

    isLow() {
        return this.props.battery.charge <= this.props.battery.low_condition;
    }
    render() {
        return (
            <div className="battery panel-item">
                <p className="title">Battery {this.isLow()
                        ? <span className="warning">Low</span>
                        : ''}</p>
                <ul>
                    <li>
                        Charge:
                        <span className="bold">{this.getCharge()}</span>
                    </li>

                    <li>
                        Condition:
                        <span className="bold">{this.getCondition()}</span>
                    </li>

                    <li className="charging">
                        Charging:
                        <span className="bold">{this.props.battery.charging
                                ? "Yes"
                                : "No"}</span>
                    </li>
                </ul>

            </div>
        );
    }
}

//map engine state to cylinder positions props
const mapStateToProps = state => {
    return {battery: state.batteryState}
}

export default connect(mapStateToProps)(Battery);
