import React, {Component} from 'react';
import {connect} from 'react-redux';

class Fueltank extends Component {

    isLow() {
        return this.props.fuel.amount <= this.props.fuel.low_condition;
    }

    getAmount() {
        return (this.props.fuel.amount * 80).toFixed(2) + "L";
    }

    isQualityGood() {
        return this.props.fuel.quality > this.props.fuel.low_quality;
    }

    render() {
        return (
            <div className="fuel panel-item">
                <p className="title">Fuel {this.isLow()
                        ? <span className="warning">Low</span>
                        : ''}
                    {!this.isQualityGood()
                        ? <span className="warning">Bad Fuel!</span>
                        : ''}</p>

                <ul>
                    <li>
                        Tank:
                        <span className="bold">
                            {this.getAmount()}
                        </span>
                    </li>
                    <li>
                        Quality:
                        <span className="bold">
                            {this.isQualityGood()
                                ? "Good"
                                : "Bad"}
                        </span>
                    </li>
                </ul>
            </div>
        );
    }
}

//map engine state to cylinder positions props
const mapStateToProps = state => {
    return {fuel: state.fuelState}
}

export default connect(mapStateToProps)(Fueltank);
