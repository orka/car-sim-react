import React, {Component} from 'react';
import {connect} from 'react-redux';

class Powerblock extends Component {

    getRPM() {
        return this
            .props
            .engine
            .rpm
            .toFixed(0);
    }

    getTiming() {
        return this
            .props
            .engine
            .timing
            .toFixed(1);
    }

    getTurns() {
        return Math.floor(this.props.engine.powerStrokesCount / 2);
    }

    getCylinderPositions() {
        return this
            .props
            .engine
            .cylindersPositions
            .toString();
    }

    getCoilFired() {
        return this
            .props
            .engine
            .ignitionCoilFire
            .toString();
    }

    getSparks() {
        return this
            .props
            .engine
            .sparkplugFire
            .toString()
    }

    getCylinderPower() {
        return this
            .props
            .engine
            .cylinderFire
            .toString()
    }

    isInRed() {
        return this.props.engine.rpm >= this.props.engine.red_rpm;
    }
    render() {
        return (
            <div className="powerblock panel-item">
                <p className="title">Powerblock V8 {this.isInRed()
                        ? <span className="warning">RED</span>
                        : ''}
                    {this.props.engine.running
                        ? <span className="note">
                                &nbsp; ON</span>
                        : ''}</p>
                <ul>
                    <li>
                        RPM:
                        <span className="bold">
                            {this.getRPM()}
                        </span>
                    </li>

                    <li>
                        Timing:
                        <span className="bold">
                            {this.getTiming()}
                        </span>
                    </li>

                    <li>
                        Strokes:
                        <span className="bold">
                            {this.props.engine.powerStrokesCount}
                        </span>
                    </li>

                    <li>
                        Turns:
                        <span className="bold">
                            {this.getTurns()}
                        </span>
                    </li>

                    <li>
                        Cylinder Positions:
                        <span className="bold">
                            {this.getCylinderPositions()}
                        </span>
                    </li>
                    <li>
                        Coils Fired:
                        <span className="bold">
                            {this.getCoilFired()}
                        </span>
                    </li>

                    <li>
                        Spark-plugs activated:
                        <span className="bold">
                            {this.getSparks()}
                        </span>
                    </li>

                    <li>
                        Cylinder power:
                        <span className="bold">
                            {this.getCylinderPower()}
                        </span>
                    </li>
                </ul>
            </div>
        );
    }
}

//map engine state to cylinder positions props
const mapStateToProps = state => {
    return {engine: state.engineState}
}

export default connect(mapStateToProps)(Powerblock);