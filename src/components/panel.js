import React, {Component} from 'react';
import {connect} from 'react-redux';
import Powerblock from './panel-powerblock';
import Battery from './panel-battery';
import Alternator from './panel-alternator';
import FuelTank from './panel-fueltank';

/**
 * Heads up display of engine components
 */
class Panel extends Component {
    constructor(props) {
        super(props);
        //
        this.state = {
            engine: 0,
            on: false
        }
    }

    render() {
        return (
            <div className="panel">
                <Powerblock/>
                <Battery/>
                <Alternator/>
                <FuelTank/>
            </div>
        );
    }
}

//map engine state to cylinder positions props
const mapStateToProps = state => {
    return {fuel: state.fuelState, engine: state.engineState, battery: state.batteryState, alternator: state.alternatorState}
}

export default connect(mapStateToProps)(Panel);
