/**
 * Animation component to help visualize power block events
 * Movement of cylinders and ignition coil action with spark-plugs event
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';

export class PowerBlockAnimation extends Component {

    createCylinders() {
        let collection = [];
        let engine = this.props.engine;
        for (let i = 0; i < engine.cylindersPositions.length; i++) {
            const stroke = engine.cylindersPositions[i];
            collection.push(
                <div
                    className={"cylinder " + i}
                    stroke={stroke}
                    key={i}
                    style={{
                    "transition": engine.timing *2 + "ms"
                }}>
                    <div
                        className="coil"
                        active={engine
                        .ignitionCoilFire
                        .indexOf(i) >= 0
                        ? "true"
                        : "false"}></div>
                    <div
                        className="sparkplug"
                        active={engine
                        .sparkplugFire
                        .indexOf(i) >= 0
                        ? "true"
                        : "false"}></div>
                    <div
                        className="piston"
                        style={{
                        "transition": engine.timing *2 + "ms"
                    }}></div>
                </div>
            )
        }

        return collection;
    }

    render() {
        return (
            <div className="power-block-visualizer">
                <p className="bold">Power block</p>
                <div className="visualizer-container">
                    {this.createCylinders()}
                    <div className="crankshaft"></div>
                </div>
            </div>
        )
    }
}

//set map functions for redux to map state to props for our component
const mapStateToProps = state => {
    return {engine: state.engineState}
}

export default connect(mapStateToProps)(PowerBlockAnimation);