import React, {Component} from 'react';
import {connect} from 'react-redux';
import {engineStart, engineStop} from '../actions/engine';

export class StartEngineButton extends Component {

    getValue() {
        return this.props.running
            ? "Stop"
            : "Start";
    }

    render() {
        return (
            <button
                className={"start-engine " + this.getValue()}
                onClick={e => {
                e.preventDefault();
                this
                    .props
                    .toggleEngine(this.props.running);
            }}>
                {this.getValue()}
            </button>
        )
    }
}

//set map functions for redux to map state to props for our component
const mapStateToProps = state => {
    return {running: state.engineState.running}
}

//map actions to dispatch
const mapDispatchToProps = dispatch => {
    return {
        toggleEngine: running => {
            if (running !== true) {
                dispatch(engineStart())
            } else {
                dispatch(engineStop())
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartEngineButton);