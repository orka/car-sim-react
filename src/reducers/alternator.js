import {SET_ALTERNATOR_CONDITION, TURN_ALTERNATOR, STOP_ALTERNATOR} from '../actions/alternator';
const initialAlternatorState = {
    running: false,
    condition: 1,
    min: 0,
    max: 1,
    low_condition: 0.8
}

const conditionCost = 0.0005;

/**
 * Transforms engine state based on actions
 * Similar to event handlers
 * @param {*} state
 * @param {*} action
 */
function alternatorState(state = initialAlternatorState, action) {
    switch (action.type) {
        case TURN_ALTERNATOR:
            return Object.assign({}, state, {
                condition: state.condition - conditionCost,
                running: true
            });
        case STOP_ALTERNATOR:
            return Object.assign({}, state, {running: false});
        case SET_ALTERNATOR_CONDITION:
            return Object.assign({}, state, {condition: action.value});
        default:
            return state;
    }
}

//export reducers
export default alternatorState;