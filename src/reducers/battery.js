import {USE_BATTERY, CHARGE_BATTERY, SET_BATTERY} from '../actions/battery';
import {ENGINE_IGNITION_COIL_FIRE} from '../actions/engine';
import {TURN_ALTERNATOR} from '../actions/alternator';
//
const initialBatteryState = {
    charge: 1,
    min: 0,
    max: 1,
    charging: false,
    condition: 1,
    low_condition: 0.8
}

const conditionCost = 0.00001;

/**
 * Transforms engine state based on actions
 * Similar to event handlers
 * @param {*} state
 * @param {*} action
 */
function batteryState(state = initialBatteryState, action) {
    switch (action.type) {
        case SET_BATTERY:
            return Object.assign({}, state, {charge: action.value});
        case USE_BATTERY:
            return Object.assign({}, state, {
                charge: Math.max(0, state.charge - action.amount),
                condition: state.condition - conditionCost,
                charging: false
            });
        case CHARGE_BATTERY:
            return Object.assign({}, state, {
                charge: Math.min(1, state.charge + action.amount * state.condition),
                condition: state.condition - conditionCost,
                charging: true
            });
        case ENGINE_IGNITION_COIL_FIRE:
            return Object.assign({}, state, {
                charge: Math.max(0, state.charge - action.energyUsage),
                condition: state.condition - conditionCost,
                charging: false
            });
        case TURN_ALTERNATOR:
            //charge
            return Object.assign({}, state, {
                charge: Math.min(1, state.charge + action.charge * state.condition),
                condition: state.condition - conditionCost,
                charging: true
            });
        default:
            return state;
    }
}

//export reducers
export default batteryState;