import {
    ENGINE_POWER_UP,
    ENGINE_POWER_DOWN,
    ENGINE_RPM,
    ENGINE_IGNITION_COIL_FIRE,
    ENGINE_SPARKPLUG_FIRE,
    ENGINE_START,
    ENGINE_STOP,
    ENGINE_CRANKSHAFT_POSITION,
    CYLINDER_FIRE
} from '../actions/engine'

const initialEngineState = {
    running: false,
    powerStrokesCount: 0,
    rpm: 0,
    red_rpm: 7000,
    timing: -1,
    min_rpm: 100,
    max_rpm: 10000,
    crankshaftPosition: 0,
    cylindersPositions: [
        0,
        1,
        2,
        3,
        0,
        1,
        2,
        3
    ],
    sparkplugFire: [],
    sparkplugFired: -1,
    ignitionCoilFire: [],
    ignitionCoilFired: -1,
    cylinderFire: [],
    cylinderFired: -1
}

function updateArray(array, item, length) {
    var collection = array.slice(0) //copy

    //remove one and add new
    collection.push(item);

    if (collection.length > length) {
        collection.shift()
    }

    return collection;
}

/**
 * Transforms engine state based on actions
 * Similar to event handlers
 * @param {*} state
 * @param {*} action
 */
function engineState(state = initialEngineState, action) {
    switch (action.type) {
        case ENGINE_START:
            return Object.assign({}, state, {running: true});
        case ENGINE_STOP:
            return Object.assign({}, state, {
                running: false,
                rpm: 0
            });
        case ENGINE_POWER_UP:
            const newRpm = state.rpm + (state.max_rpm - state.min_rpm) * action.amount;

            return Object.assign({}, state, {
                rpm: Math.min(state.max_rpm, Math.max(state.min_rpm, newRpm))
            });
        case ENGINE_POWER_DOWN:
            var rpm = state.rpm - (state.max_rpm - state.min_rpm) * action.amount;
            return Object.assign({}, state, {
                rpm: Math.min(state.max_rpm, Math.max(state.min_rpm, rpm))
            });
        case ENGINE_RPM:
            return Object.assign({}, state, {
                rpm: Math.min(state.max_rpm, Math.max(state.min_rpm, action.value))
            });
        case ENGINE_CRANKSHAFT_POSITION:
            return Object.assign({}, state, {
                crankshaftPosition: action.crankshaftPosition,
                cylindersPositions: action.cylindersPositions,
                rpm: action.rpm,
                timing: action.timing,
                powerStrokesCount: state.powerStrokesCount + 1
            })
        case ENGINE_IGNITION_COIL_FIRE:
            return Object.assign({}, state, {
                ignitionCoilFire: updateArray(state.ignitionCoilFire, action.position, 2),
                ignitionCoilFired: action.position
            })
        case ENGINE_SPARKPLUG_FIRE:
            return Object.assign({}, state, {
                sparkplugFire: updateArray(state.sparkplugFire, action.position, 2),
                sparkplugFired: action.position
            });
        case CYLINDER_FIRE:
            //assign new state
            return Object.assign({}, state, {
                cylinderFire: updateArray(state.cylinderFire, action.position, 2),
                cylinderFired: action.position
            });
        default:
            return state;
    }
}

//export reducers
export default engineState;