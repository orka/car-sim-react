import {USE_GAS, ADD_GAS, SET_FUEL} from '../actions/gas-tank';
//
const initialFuelState = {
    amount: 1,
    min: 0,
    max: 1,
    low_condition: 0.2,
    quality: 1,
    low_quality: 0.8
}

/**
 * Transforms engine state based on actions
 * Similar to event handlers
 * @param {*} state
 * @param {*} action
 */
function fuelState(state = initialFuelState, action) {
    switch (action.type) {
        case USE_GAS:
            return Object.assign({}, state, {
                amount: Math.max(0, state.amount - action.amount)
            });
        case ADD_GAS:
            return Object.assign({}, state, {
                amount: Math.min(1, state.amount + action.amount)
            });
        case SET_FUEL:
            return Object.assign({}, state, {
                amount: Math.min(1, Math.max(0, action.amount))
            });
        default:
            return state;
    }
}

//export reducers
export default fuelState;