import {combineReducers} from 'redux';
import batteryState from './battery';
import engineState from './engine';
import alternatorState from './alternator';
import fuelState from './gas-tank';
//combine reducers
const reducers = combineReducers({batteryState, engineState, alternatorState, fuelState})

//export reducers
export default reducers;